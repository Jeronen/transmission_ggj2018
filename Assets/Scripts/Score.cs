﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	public int TotalScore;
	public AudioClip ScoreIncreasedAudio;

	private Text scoreText;
	private AudioSource audioSource;
	

	private void Start() {
		scoreText = GetComponent<Text>();
		audioSource = GetComponent<AudioSource>();
	}

	public void IncreaseScore() {
		TotalScore += 100;
		scoreText.text = string.Format("Score: {0}", TotalScore);
		audioSource.PlayOneShot(ScoreIncreasedAudio, 1F);

	}
}
