﻿using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 using UnityEngine.UI;
 
 public class Animate : MonoBehaviour {
 
     public float Speed = 0.02f;
     public float MaxSize = 1f;
     public float MinSize = 0.9f;
     
     private float currentRatio = 1;
     private Text text;
 
     private TransmissionData transmissionData;
 
     void Awake () {
         text = gameObject.GetComponentInChildren<Text>();
         transmissionData = GetComponent<TransmissionData>();
         StartCoroutine(this.Pulse());
     }

     IEnumerator Pulse(){
         while (true){
             while (this.currentRatio != this.MaxSize){
                 currentRatio = Mathf.MoveTowards( currentRatio, MaxSize, Speed);
                 text.transform.localScale = Vector3.one * currentRatio;
                 yield return new WaitForEndOfFrame();
             }
 
             while (currentRatio != MinSize){
                 currentRatio = Mathf.MoveTowards( currentRatio, MinSize, Speed);
                 text.transform.localScale = Vector3.one * currentRatio;
                 yield return new WaitForEndOfFrame();
             }
             if (transmissionData.Selected) {
                 text.color = Color.green;
             }
             else {
                 text.color = Color.white;
             }
         }
     }
 }