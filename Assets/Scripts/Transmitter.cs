﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Transmitter : MonoBehaviour {

	public AudioClip[] WinAudios;
	public AudioClip FailAudio;
	public float AudioDelay = 30f;
	
	public Score Score;
	
	public Text ValueText;	
	public GameObject[] ValueContainers;
	public GameObject ValueFactory;
	
	private const float volumeScale = 1F;

	private int value;
	private int sum;
	private AudioSource audioSource;
	private bool gameOver;

	private void Start() {
		ValueContainers = GameObject.FindGameObjectsWithTag("ValueContainer");
		audioSource = GetComponent<AudioSource>();
		ValueFactory = GameObject.FindGameObjectWithTag("ValueFactory");
	}

	private IEnumerator CheckGameState() {
		yield return new WaitForSeconds(10f);
		if (gameOver) {
			DoGameOver();			
		}
	}

	public void CalculateSum() {
		int.TryParse(ValueText.text, out value);
		
		foreach (var valueContainer in ValueContainers) {
			if (valueContainer.GetComponent<TransmissionData>().Selected) {
				valueContainer.GetComponent<TransmissionData>().Selected = false;
				int containerValue;
				int.TryParse(valueContainer.GetComponentInChildren<Text>().text, out containerValue);
				sum += containerValue;
			}
		}
		UpdateGameState();
	}

	private void UpdateGameState() {
		if (sum.Equals(value)) {

			var selectedAudio = Random.Range(0, WinAudios.Length);
			audioSource.clip = WinAudios[selectedAudio];
			audioSource.PlayDelayed(1f);
			Score.IncreaseScore();
			ValueFactory.GetComponent<ValueFactory>().InitValues();
		}
		else {
			audioSource.clip = FailAudio;
			audioSource.PlayDelayed(1f);
			gameOver = true;
			print("game over son");
			StartCoroutine(CheckGameState());
		}
		sum = 0;
	}

	private void DoGameOver() {
		SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
	}
}
