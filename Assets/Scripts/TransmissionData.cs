﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransmissionData : MonoBehaviour {
     public bool Selected;
     public Text Value;

    public void ToggleSelected() {
       Selected = !Selected;
    }
}
