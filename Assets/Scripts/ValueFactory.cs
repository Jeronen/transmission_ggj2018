﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueFactory : MonoBehaviour {
	public int AmountOfTransmitterValueParts;
	
	private GameObject[] valueContainers;
	private GameObject transmitter;
	private int transmitterValue;
	private List<int> transmitterValueParts;
	
	void Start () {
		GetGameObjects();
		InitValues();
	}

	public void InitValues() {
		InitTransmitterValue();
		InitValueContainerValues();
	}

	private void GetGameObjects() {
		transmitter = GameObject.FindGameObjectWithTag("Transmitter");
		valueContainers = GameObject.FindGameObjectsWithTag("ValueContainer");
	}

	private void InitTransmitterValue() {
		InitTransmitterValueParts();
		foreach (var transmitterValuePart in transmitterValueParts) {
			transmitterValue += transmitterValuePart;
		}
		transmitter.GetComponent<Transmitter>().ValueText.text = transmitterValue.ToString();
	}
	
	private void InitTransmitterValueParts() {
		transmitterValueParts = new List<int>();
		for (var i = 0; i < AmountOfTransmitterValueParts; i++) {
			transmitterValueParts.Add(Random.Range(1, 9)* 10);
		}
	}
	
	private void InitValueContainerValues() {
		for (var i = 0; i < AmountOfTransmitterValueParts; i++) {
			valueContainers[i].GetComponentInChildren<Text>().text = transmitterValueParts[i].ToString();
		}
		foreach (var valueContainer in valueContainers) {
			if (valueContainer.GetComponentInChildren<Text>().text.Length == 0 ||
			    valueContainer.GetComponentInChildren<Text>().text.Equals("0")) {
				var randomValue = Random.Range(1, 9) * 10;
				valueContainer.GetComponentInChildren<Text>().text = randomValue.ToString();
			}
		}
	}
}
